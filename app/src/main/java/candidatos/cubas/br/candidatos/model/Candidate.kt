package candidatos.cubas.br.candidatos.model

data class Candidate(
    val id: Long = 0,
    val name: String = "",
    val party: String = "",
    val details: String = "",
    val picture: String = "",
    val site: String = "",
    val offer: String = "",
    val vote: Int = 0,
    val percentageVotes: Double = 0.0
)