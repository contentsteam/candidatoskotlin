package br.cubas.acessorest

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import candidatos.cubas.br.candidatos.R
import candidatos.cubas.br.candidatos.model.Candidate
import com.koushikdutta.ion.Ion
import kotlinx.android.synthetic.main.candidate_line.view.*

class CandidateListAdapter(private val movies: List<Candidate>?,
                       private val context: Context) : Adapter<CandidateListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.candidate_line, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        movies?.let {
            return it.size
        }
        return 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = movies?.get(position)
        movie?.let { holder.bindView(it) }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(candidate: Candidate) {
            val title = itemView.movie_item_title
            val type = itemView.movie_item_type
            val image = itemView.move_image
            title.text = candidate.name
            type.text = candidate.party
            Ion.with(image).load(candidate.picture)
        }

    }
}

