package br.cubas.acessorest.utils

import candidatos.cubas.br.candidatos.model.Candidate
import com.beust.klaxon.Klaxon
import java.net.URL

object Util {

    fun parse(path: String?): List<Candidate>?{
        val contents = URL(path).readText()
        val candidates = contents?.let { Klaxon().parse<List<Candidate>>(it) }
        return candidates
    }

}