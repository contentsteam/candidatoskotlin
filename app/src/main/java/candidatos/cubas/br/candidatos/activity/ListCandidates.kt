package candidatos.cubas.br.candidatos.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import br.cubas.acessorest.CandidateListAdapter
import br.cubas.acessorest.api.ApiClient
import br.cubas.acessorest.api.ApiService
import candidatos.cubas.br.candidatos.R
import candidatos.cubas.br.candidatos.model.Candidate

import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListCandidates : AppCompatActivity() {

    var recyclerView: RecyclerView?= null
    var progressBarWaiting: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_candidates)
    }

    override fun onResume() {
        super.onResume()
        getCandidates()
    }

    private fun getCandidates() {
        var apiServices = ApiClient.getClient().create(ApiService::class.java)

        var call = apiServices?.listCandidates("application/json")
        call?.enqueue(object : Callback<List<Candidate>> {

            init {
                progressBarWaiting?.visibility = View.VISIBLE
            }

            override fun onResponse(call: Call<List<Candidate>>, response: Response<List<Candidate>>) {
                if (response.isSuccessful) {
                    var candidates = response.body()
                    recyclerView?.adapter = CandidateListAdapter(candidates, applicationContext)
                    progressBarWaiting?.visibility = View.INVISIBLE
                }
                progressBarWaiting?.visibility = View.INVISIBLE
            }

            override fun onFailure(call: Call<List<Candidate>>, t: Throwable) {
                progressBarWaiting?.visibility = View.INVISIBLE
                alert("Erro ao tentar ler os filmes") {
                    title = "Erro"
                    negativeButton("ok") {toast("Erro!!!")}
                }.show()
            }
        })
    }
}