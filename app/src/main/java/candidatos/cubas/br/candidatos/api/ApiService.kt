package br.cubas.acessorest.api

import candidatos.cubas.br.candidatos.model.Candidate
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiService {

    @GET("/candidato/list?r=json")
    fun listCandidates(@Header("Content-Type") content_type: String): Call<List<Candidate>>
}